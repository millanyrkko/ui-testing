const expect = require('chai').expect;

const { Builder, By, Key, WebElement, Browser } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
require('geckodriver');

const BASE_URL = "https://www.levykauppax.fi/";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

describe('UI Tests', () => {
    /** @type {import('selenium-webdriver').ThenableWebDriver} */
    let driver = undefined;
    before(async () => {
        // firefox options
        const options = new firefox.Options(); // firefox settings
        options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe"); // path to firefox.exe
        // webdriver creation
        driver = await new Builder()
            .forBrowser(Browser.FIREFOX)
            .setFirefoxOptions(options) // setting options
            .build();
        await driver.get(BASE_URL);
        await sleep(5);
    });
    it('Can find search bar', async () => {
        const searchbar = await driver.findElement(By.id("q"));
    });
    it('Can get and set "q" input field (searchbar)', async () => {
        const searchbox = await driver.findElement(By.id("q"));
        let search_value = await searchbox.getAttribute('value');
        expect(search_value).to.eq("");
        // modify the search_value
        const keystroke_sequence = [];
        keystroke_sequence.push("nomad"); // user writes value
        keystroke_sequence.push(Key.ENTER); // user presses enter
        await searchbox.sendKeys(...keystroke_sequence);
        search_value = await searchbox.getAttribute('value');
        expect(search_value).to.eq("nomad");
        await sleep(5);
    });
    after(async () => {
        await driver.close();
    });
});
